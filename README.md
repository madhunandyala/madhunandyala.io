# madhunandyala.io


Madhu Nandyala
Flat No 201, MIG 757, Sairam Enclave,
KPHB Colony Phase 2 Hyderabad-500072
91 9966869555
MADHUNANDYALA@HOTMAIL.COM


TECHNICAL EXPERTISE
➔	Around 12 years of experience in the IT industry as a consultant and Team Lead at various levels on Application Security Vulnerability Assessment and remediation, Component Lifecycle Management and Threat modelling.
➔	Strong Vulnerability Assessment, Analysis and Remediation techniques using static tools (SAST) like HPE Fortify, Checkmarx, Veracode and IBM AppScan. 
➔	Taking care of SAST, SCA and DevSecOps related activities at the current role.
➔	Leading AppSec estate maintenance (SAST, SCA and GITHUB Actions server maintaining) for the Organization. This includes Installation, Monitoring, Upgrading tools and Patching tools as well.  My team was responsible for all the Application Security related tasks i.e, Threat Modelling, SAST, DAST, FOSS (SCA) efforts. I am working as a Security Focal from Offshore. 
➔	Good understanding of the Security risks associated with Free and Open Source Software (FOSS) and their mitigation techniques (by patching/rewriting/upgrading the components). 
➔	Sound knowledge on OWASP top 10 2017 & 2013 vulnerability and remediation techniques.
➔	Proven Application Security tooling estate experience involving activities like Tool evaluation, implementation, integration with DevOps and Maintenance of the tooling estate etc.
➔	Strong experience on DevOps tools like GITHUB, Subversion (SVN), Hudson, Docker and Kubernetes
➔	Involved in early stages of project implementation to bring Security flavor by using Design thinking and Threat modelling strategies and tools like SecurebyDesign by Compass Security and IriusRisk.
➔	Took the whole responsibility of setting up a Lab for DevSecOps for educating practitioners across the organization.
➔	Integration of Security tools like Checkmarx, IBM AppScan, MicroFocus Fortify, Sonatype Nexus IQ Server etc. with Jenkins CI/CD build pipelines (Secure DevOps).


SKILLS
Application Security related tools :	IBM AppScan, MF Fortify, Checkmarx, Veracode, Sonatype CLM Nexus IQ Server, SonarQube, Secure by Design Elements, Burp Intercepting tool, Dynamic testing of web applications using AppScan and NetSparker.
DevOps tools	: Jenkins, GITHUB Actions, GITLAB CICD, AWS Code pipeline etc
Other Tools	: Seemplicity, IDEs like Eclipse, MS Visual Studio code, Apache Maven.
Programming languages	Java SE and Java EE, PL SQL, NodeJS, Python (beginner)
